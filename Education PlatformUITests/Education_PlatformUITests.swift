//
//  Education_PlatformUITests.swift
//  Education PlatformUITests
//
//  Created by Aníbal Pagán Ventura on 10/2/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import XCTest
import FirebaseAuth

class Education_PlatformUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    struct CorrectLogin {
        static var email: String = "anibal@test.com"
        static var password: String = "Junior1997!"
        
        func login(app: XCUIApplication) {
            let loginEmailTextField = app.textFields["Login Email Text Field"]
            let loginPasswordTextField = app.secureTextFields["Login Password Text Field"]
            
            loginEmailTextField.tap()
            loginEmailTextField.typeText(CorrectLogin.email)
            loginPasswordTextField.tap()
            loginPasswordTextField.typeText(CorrectLogin.password)
            app.buttons["Login Button"].tap()
        }
    }
    
    struct CorrectSignup {
        static var password: String = "Test123!"
        
        func randomEmail() -> String {
            let nameLength = Int.random(in: 5 ... 10)
            let domainLength = Int.random(in: 5 ... 10)
            let domainSuffixes = ["com", "net", "org", "io", "co.uk"]
            let name = randomString(length: nameLength)
            let domain = randomString(length: domainLength)
            let randomDomainSuffixIndex = Int.random(in: 0 ... 4)
            let domainSuffix = domainSuffixes[randomDomainSuffixIndex]
            let text = name + "@" + domain + "." + domainSuffix
            return text
        }
        
        func randomName() -> String {
            let nameLength = Int.random(in: 5 ... 10)
            return randomString(length: nameLength)
        }
        
        func randomString(length: Int) -> String {
          let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
          return String((0..<length).map{ _ in letters.randomElement()! })
        }
    }

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        app.terminate()
    }

    func testIncorrectEmailOnLogin() {
        let loginEmailTextField = app.textFields["Login Email Text Field"]
        let loginPasswordTextField = app.secureTextFields["Login Password Text Field"]
        
        XCTAssertTrue(loginEmailTextField.exists, "Login email text field not found")
        XCTAssertTrue(loginPasswordTextField.exists, "Login password text field not found")
        
        loginEmailTextField.tap()
        loginEmailTextField.typeText("wrongemail@lol.com")
        loginPasswordTextField.tap()
        loginPasswordTextField.typeText(CorrectLogin.password)
        app.buttons["Login Button"].tap()
        
        XCTAssertTrue(app.staticTexts["Login Error Label"].label == "Wrong email or password.", "Wrong or missing text in login error label")
        
    }
    
    func testIncorrectPasswordOnLogin() {
        let loginEmailTextField = app.textFields["Login Email Text Field"]
        let loginPasswordTextField = app.secureTextFields["Login Password Text Field"]
        
        XCTAssertTrue(loginEmailTextField.exists, "Login email text field not found")
        XCTAssertTrue(loginPasswordTextField.exists, "Login password text field not found")
        
        loginEmailTextField.tap()
        loginEmailTextField.typeText(CorrectLogin.email)
        loginPasswordTextField.tap()
        loginPasswordTextField.typeText("incorrectpassword")
        app.buttons["Login Button"].tap()
        
        XCTAssertTrue(app.staticTexts["Login Error Label"].label == "Wrong email or password.", "Wrong or missing text in login error label")
    }

}
