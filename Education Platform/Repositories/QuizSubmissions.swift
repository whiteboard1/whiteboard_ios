import FirebaseFirestore

class QuizSubmissions {

    static var QUIZ_SUBMISSION_COLLECTION = "quiz_submissions"
    static var STUDENT_ID = "student_id"
    static var QUIZ_ID = "quiz_id"
    static var SUBMITTED_DATE = "submitted_date"

    static func getAllByQuiz(quizId: String, onSuccess: @escaping ([QuizSubmission]) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        db.collection(QUIZ_SUBMISSION_COLLECTION)
            .whereField(QUIZ_ID, isEqualTo: quizId)
            .getDocuments() {(querySnapshot, err) in 
                if let err = err {
                    onFailure(err)
                } else {
                    var list: [QuizSubmission] = []
                    for document in querySnapshot!.documents {
                        let qs = parseDataToQuizSub(data: document.data())
                        qs.id = document.documentID
                        list.append(qs)
                    }
                    
                    onSuccess(list)
                }
            }
    }

    static func getAllByStudent(studentId: String, onSuccess: @escaping ([QuizSubmission]) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        db.collection(QUIZ_SUBMISSION_COLLECTION)
            .whereField(STUDENT_ID, isEqualTo: studentId)
            .getDocuments() {(querySnapshot, err) in 
                if let err = err {
                    onFailure(err)
                } else {
                    var list: [QuizSubmission] = []
                    for document in querySnapshot!.documents {
                        let qs = parseDataToQuizSub(data: document.data())
                        qs.id = document.documentID
                        list.append(qs)
                    }
                    
                    onSuccess(list)
                }
            }
    }

    static func add(quizSub: QuizSubmission, onSuccess: @escaping (QuizSubmission) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        var data: [String : Any] = [:]
        if let studentId = quizSub.studentId, let quizId = quizSub.quizId {
            data = [
                STUDENT_ID: studentId,
                QUIZ_ID: quizId,
                SUBMITTED_DATE: FieldValue.serverTimestamp()
            ]
        } else {
            onFailure(RError(msg: "Student id and quiz id must not be nil"))
        }

        var ref: DocumentReference? = nil
        ref = db.collection(QUIZ_SUBMISSION_COLLECTION)
            .addDocument(data: data) { err in
                if let err = err {
                    onFailure(err)
                } else {
                    quizSub.id = ref?.documentID
                    onSuccess(quizSub)
                }
            }
    }

    static func delete(quizSub: QuizSubmission, parameters: [String]?, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        if let id = quizSub.id {
            db.collection(QUIZ_SUBMISSION_COLLECTION).document(id)
                .delete(){ err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
        } else {
            onFailure(RError(msg: "QuizSubmission id must not be nil"))
        }
    }

    private static func parseDataToQuizSub(data: [String : Any]) -> QuizSubmission {
        let qs = QuizSubmission()
        qs.studentId = data[STUDENT_ID] as! String
        qs.quizId = data[QUIZ_ID] as! String
        qs.submittedDate = (data[SUBMITTED_DATE] as! Timestamp).dateValue()

        return qs
    }
}
