import FirebaseFirestore

class Questions{

    static var QUESTION_COLLECTION = "questions"
    static var QUIZ_ID = "quiz_id"
    static var TEXT = "text"
    static var POINTS = "points"

    static func getAllByQuiz(quizId: String, onSuccess: @escaping ([Question]) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        db.collection(QUESTION_COLLECTION)
            .whereField(QUIZ_ID, isEqualTo: quizId)
            .getDocuments() {(querySnapshot, err) in 
                if let err = err {
                    onFailure(err)
                } else {
                    var list: [Question] = []
                    for document in querySnapshot!.documents {
                        let question = parseDataToQuestion(data: document.data())
                        question.id = document.documentID
                        list.append(question)
                    }
                    
                    onSuccess(list)
                }
            }
    }

    static func add(question: Question, onSuccess: @escaping (Question) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        var data: [String : Any] = [:]
        if let quizId = question.quizId {
            data = [
                QUIZ_ID: quizId,
                TEXT: question.text,
                POINTS: question.points
            ]
        } else {
            onFailure(RError(msg: "Quiz id of question must not be nil"))
        }

        var ref: DocumentReference? = nil
        ref = db.collection(QUESTION_COLLECTION)
            .addDocument(data: data) { err in
                if let err = err {
                    onFailure(err)
                } else {
                    question.id = ref?.documentID
                    onSuccess(question)
                }
            }
    }

    static func update(question: Question, parameters: [String]?, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        var data: [String : Any] = [:]
        if let parameters = parameters {
            for param in parameters {
                switch param {
                    case TEXT:
                        data[param] = question.text
                    case POINTS:
                        data[param] = question.points
                    default:
                        onFailure(RError(msg: "Invalid parameter"))
                        return
                }
            }
        } else {
            data = [
                TEXT: question.text,
                POINTS: question.points
            ]
        }

        if let id = question.id {
            db.collection(QUESTION_COLLECTION).document(id)
                .updateData(data){ err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
        } else {
            onFailure(RError(msg: "Quiz id must not be nil"))
        }
    }

    static func delete(question: Question, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        if let id = question.id {
            db.collection(QUESTION_COLLECTION).document(id)
                .delete(){ err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
        } else {
            onFailure(RError(msg: "Question id must not be nil"))
        }
    }

    private static func parseDataToQuestion(data: [String : Any]) -> Question {
        let q = Question(text: data[TEXT] as! String, points: data[POINTS] as! Int)
        q.quizId = data[QUIZ_ID] as! String
        return q
    }
}
