import FirebaseFirestore

class Quizzes {

    static var QUIZ_COLLECTION = "quizzes"
    static var ID = "id"
    static var NAME = "name"
    static var DURATION = "duration"
    static var SUBJECT = "subject"
    static var CREATOR = "creator"
    static var CREATOR_ID = "creator_id"
    static var OPEN_DATE = "open_date"
    static var CLOSE_DATE = "close_date"
    static var PARTIAL_POINTS_ALLOWED = "partial_points_allowed"
    static var CREATION_DATE = "creation_date"
    static var ENROLLMENT_KEY = "enrollment_key"


    static func getAllByCreatorId(creatorId: String, onSuccess: @escaping ([Quiz]) -> Void, onFailure: @escaping (Error) -> Void) {
        let db = Firestore.firestore()

        db.collection(QUIZ_COLLECTION)
            .whereField(CREATOR_ID, isEqualTo: creatorId)
            .order(by: CREATION_DATE, descending: true)
            .getDocuments() { (querySnapshot, error) in 
                if let e = error {
                    onFailure(e)
                } else{
                    var list: [Quiz] = []
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        let q = parseDataToQuiz(data: data)
                        q.id = document.documentID
                        list.append(q)
                    }

                    onSuccess(list)
                }
            }
    }

    static func getById(id: String, onSuccess: @escaping (Quiz) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        db.collection(QUIZ_COLLECTION).document(id)
            .getDocument { (document, error) in 
                if let document = document, document.exists {
                    let quiz = parseDataToQuiz(data: document.data()!)
                    quiz.id = document.documentID
                    onSuccess(quiz)
                } else {
                    onFailure(error!)
                }
            }
    }

    static func add(quiz: Quiz, onSuccess: @escaping (Quiz) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        var data: [String : Any] = [:]
        if let creatorId = quiz.creatorId {
            data = [
                NAME: quiz.name,
                SUBJECT: quiz.subject,
                DURATION: quiz.duration,
                CREATOR: quiz.creator,
                CREATION_DATE: FieldValue.serverTimestamp(),
                CREATOR_ID: creatorId,
                OPEN_DATE: quiz.openDate,
                CLOSE_DATE: quiz.closeDate,
                PARTIAL_POINTS_ALLOWED: quiz.partialPointsAllowed
            ]
        } else {
            onFailure(RError(msg: "Creator id of quiz cannot be nil"))
            return 
        }

        var ref: DocumentReference? = nil
        ref = db.collection(QUIZ_COLLECTION)
            .addDocument(data: data) { err in
                if let err = err {
                    onFailure(err)
                } else {
                    quiz.id = ref?.documentID
                    onSuccess(quiz)
                }
            }
    }

    static func update(quiz: Quiz, parameters: [String]?, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        var data: [String : Any] = [:]

        if let parameters = parameters {
            for param in parameters {
                switch param {
                    case NAME:
                        data[param] = quiz.name
                    case DURATION:
                        data[param] = quiz.duration
                    case SUBJECT:
                        data[param] = quiz.subject
                    case CREATOR:
                        data[param] = quiz.creator
                    case OPEN_DATE:
                        data[param] = quiz.openDate
                    case CLOSE_DATE:
                        data[param] = quiz.closeDate
                    case PARTIAL_POINTS_ALLOWED:
                        data[param] = quiz.partialPointsAllowed
                    case ENROLLMENT_KEY:
                        data[param] = quiz.enrollmentKey
                    default:
                        onFailure(RError(msg: "Invalid parameter"))
                        return
                }
            }
        } else {
            data = [
                NAME: quiz.name,
                SUBJECT: quiz.subject,
                DURATION: quiz.duration,
                CREATOR: quiz.creator,
                OPEN_DATE: quiz.openDate,
                CLOSE_DATE: quiz.closeDate,
                PARTIAL_POINTS_ALLOWED: quiz.partialPointsAllowed
            ]
        }

        if let id = quiz.id {
            db.collection(QUIZ_COLLECTION).document(id)
                .updateData(data){ err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
        } else {
            onFailure(RError(msg: "Quiz id must not be nil"))
        }
        
    }

    static func delete(quiz: Quiz, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        if let id = quiz.id {
            db.collection(QUIZ_COLLECTION).document(id)
                .delete(){ err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
        } else {
            onFailure(RError(msg: "Quiz id must not be nil"))
        }
    }

    private static func parseDataToQuiz(data: [String : Any]) -> Quiz {
        let q = Quiz(name: data[NAME] as! String, subject: data[SUBJECT] as! String, creator: data[CREATOR] as! String, duration: data[DURATION] as! Int, partialPointsAllowed: data[PARTIAL_POINTS_ALLOWED] as! Bool)
        q.openDate = (data[OPEN_DATE] as! Timestamp).dateValue()
        q.closeDate = (data[CLOSE_DATE] as! Timestamp).dateValue()
        q.creatorId = data[CREATOR_ID] as! String
        q.creationDate = (data[CREATION_DATE] as? Timestamp)?.dateValue()
        if let key = data[ENROLLMENT_KEY] as? String{
            q.enrollmentKey = key
        }
        return q
    }
}
