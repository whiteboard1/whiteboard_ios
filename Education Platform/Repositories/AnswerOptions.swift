import FirebaseFirestore

class AnswerOptions {

    static var ANSWER_OPTIONS_COLLECTION = "answer_options"
    static var QUESTION_ID = "question_id"
    static var TEXT = "text"
    static var IS_CORRECT = "is_correct"
    static var EXPLANATION = "explanation"

    static func getAllByQuestion(questionId: String, onSuccess: @escaping ([AnswerOption]) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        db.collection(ANSWER_OPTIONS_COLLECTION)
            .whereField(QUESTION_ID, isEqualTo: questionId)
            .getDocuments() {(querySnapshot, err) in 
                if let err = err {
                    onFailure(err)
                } else {
                    var list: [AnswerOption] = []
                    for document in querySnapshot!.documents {
                        let ao = parseDataToAnsOpt(data: document.data())
                        ao.id = document.documentID
                        list.append(ao)
                    }
                    
                    onSuccess(list)
                }
            }
    }

    static func add(ansOpt: AnswerOption, onSuccess: @escaping (AnswerOption) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        var data: [String : Any] = [:]
        if let questionId = ansOpt.questionId {
            data = [
                QUESTION_ID: questionId,
                TEXT: ansOpt.text,
                IS_CORRECT: ansOpt.isCorrect,
                EXPLANATION: ansOpt.explanation
            ]
        } else {
            onFailure(RError(msg: "Quiz id of question must not be nil"))
        }

        var ref: DocumentReference? = nil
        ref = db.collection(ANSWER_OPTIONS_COLLECTION)
            .addDocument(data: data) { err in
                if let err = err {
                    onFailure(err)
                } else {
                    ansOpt.id = ref?.documentID
                    onSuccess(ansOpt)
                }
            }
    }

    static func update(ansOpt: AnswerOption, parameters: [String]?, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        var data: [String : Any] = [:]
        if let parameters = parameters {
            for param in parameters {
                switch param {
                    case TEXT:
                        data[param] = ansOpt.text
                    case IS_CORRECT:
                        data[param] = ansOpt.isCorrect
                    case EXPLANATION:
                        data[param] = ansOpt.explanation
                    default:
                        onFailure(RError(msg: "Invalid parameter"))
                        return
                }
            }
        } else {
            data = [
                TEXT: ansOpt.text,
                IS_CORRECT: ansOpt.isCorrect,
                EXPLANATION: ansOpt.explanation
            ]
        }

        if let id = ansOpt.id {
            db.collection(ANSWER_OPTIONS_COLLECTION).document(id)
                .updateData(data){ err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
        } else {
            onFailure(RError(msg: "AnswerOption id must not be nil"))
        }
    }

    static func delete(ansOpt: AnswerOption, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        if let id = ansOpt.id {
            db.collection(ANSWER_OPTIONS_COLLECTION).document(id)
                .delete(){ err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
        } else {
            onFailure(RError(msg: "AnswerOption id must not be nil"))
        }
    }

    private static func parseDataToAnsOpt(data: [String : Any]) -> AnswerOption{
        let ao = AnswerOption(text: data[TEXT] as! String, isCorrect: data[IS_CORRECT] as! Bool, explanation: data[EXPLANATION] as! String)
        ao.questionId = data[QUESTION_ID] as! String

        return ao
    }
}
