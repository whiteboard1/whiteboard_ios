import FirebaseFirestore

class AnswerSubmissions {

    static var ANSWER_SUBMISSION_COLLECTION = "answer_submissions"
    static var QUIZ_SUBMISSION_ID = "quiz_submission_id"
    static var QUESTION_ID = "question_id"
    static var SUBMITTED_ANSWER = "submitted_answer"
    static var SCORE = "score"
    static var TIME_TO_ANSWER = "time_to_answer"

    static func getAllByQuizSubmission(quizSubId: String, onSuccess: @escaping ([AnswerSubmission]) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        db.collection(ANSWER_SUBMISSION_COLLECTION)
            .whereField(QUIZ_SUBMISSION_ID, isEqualTo: quizSubId)
            .getDocuments() {(querySnapshot, err) in 
                if let err = err {
                    onFailure(err)
                } else {
                    var list: [AnswerSubmission] = []
                    for document in querySnapshot!.documents {
                        let asub = parseDataToAnsSub(data: document.data())
                        asub.id = document.documentID
                        list.append(asub)
                    }
                    
                    onSuccess(list)
                }
            }
    }

    static func getAllByQuestion(questionId: String, onSuccess: @escaping ([AnswerSubmission]) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        db.collection(ANSWER_SUBMISSION_COLLECTION)
            .whereField(QUESTION_ID, isEqualTo: questionId)
            .getDocuments() {(querySnapshot, err) in 
                if let err = err {
                    onFailure(err)
                } else {
                    var list: [AnswerSubmission] = []
                    for document in querySnapshot!.documents {
                        let asub = parseDataToAnsSub(data: document.data())
                        asub.id = document.documentID
                        list.append(asub)
                    }
                    
                    onSuccess(list)
                }
            }
    }

    static func add(answerSub: AnswerSubmission, onSuccess: @escaping (AnswerSubmission) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        var data: [String : Any] = [:]
        if let questionId = answerSub.questionId, let quizSubId = answerSub.quizSubmissionId {
            data = [
                QUIZ_SUBMISSION_ID: quizSubId,
                QUESTION_ID: questionId,
                SUBMITTED_ANSWER: answerSub.submittedAnswer,
                SCORE: answerSub.score,
                TIME_TO_ANSWER: answerSub.timeToAnswer
            ]
        } else {
            onFailure(RError(msg: "Quiz id and question id must not be nil"))
        }

        var ref: DocumentReference? = nil
        ref = db.collection(ANSWER_SUBMISSION_COLLECTION)
            .addDocument(data: data) { err in
                if let err = err {
                    onFailure(err)
                } else {
                    answerSub.id = ref?.documentID
                    onSuccess(answerSub)
                }
            }
    }

    static func update(answerSub: AnswerSubmission, parameters: [String]?, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        var data: [String : Any] = [:]
        if let parameters = parameters {
            for param in parameters {
                switch param {
                    case SUBMITTED_ANSWER:
                        data[param] = answerSub.submittedAnswer
                    case SCORE:
                        data[param] = answerSub.score
                    case TIME_TO_ANSWER:
                        data[param] = answerSub.timeToAnswer
                    default:
                        onFailure(RError(msg: "Invalid parameter"))
                        return
                }
            }
        } else {
            data = [
                SUBMITTED_ANSWER: answerSub.submittedAnswer,
                SCORE: answerSub.score,
                TIME_TO_ANSWER: answerSub.timeToAnswer
            ]
        }

        if let id = answerSub.id {
            db.collection(ANSWER_SUBMISSION_COLLECTION).document(id)
                .updateData(data){ err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
        } else {
            onFailure(RError(msg: "AnswerSubmission id must not be nil"))
        }
    }

    static func delete(answerSub: AnswerSubmission, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        if let id = answerSub.id {
            db.collection(ANSWER_SUBMISSION_COLLECTION).document(id)
                .delete(){ err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
        } else {
            onFailure(RError(msg: "AnswerSubmission id must not be nil"))
        }
    }

    private static func parseDataToAnsSub(data: [String : Any]) -> AnswerSubmission {
        let asub = AnswerSubmission(submittedAnswer: data[SUBMITTED_ANSWER] as! [String], score: data[SCORE] as! Double, timeToAnswer: data[TIME_TO_ANSWER] as! Int)
        asub.questionId = data[QUESTION_ID] as! String
        asub.quizSubmissionId = data[QUIZ_SUBMISSION_ID] as! String

        return asub
    }
}
