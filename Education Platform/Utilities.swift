//
//  Utilities.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 10/29/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import Foundation

class Utilities {
    
    /********************************************************************/
    /******                                                    String Bank                                                      ******/
    /********************************************************************/
    static let errors: [Int:String] = [1 : "Wrong email format.",
                                       2 : "Password must contain one uppercase letter.",
                                       3 : "Password must contain one lowercase letter.",
                                       4 : "Password must contain a special character.",
                                       5 : "Passwords must match.",
                                       6 : "Wrong email or password.",
                                       7 : "User not found.",
                                       8 : "You Must fill in every field.",
                                       9 : "Password must be 8 to 20 characters."]
    
    static let options: [String] = ["A:", "B:", "C:", "D:", "E:", "F:", "G:", "G:", "I:", "J:", "K:", "L:", "M:", "N:", "O:", "P:", "Q:", "R:", "S:", "T:", "U:", "V:", "W:", "X:", "Y:", "Z:"]
    
}
