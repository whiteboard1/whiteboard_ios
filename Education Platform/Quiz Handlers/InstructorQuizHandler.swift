//
//  InstructorQuizBuilder.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 12/3/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import Foundation

class InstructorQuizHandler {
    
    static func getCreatedQuizzes(creatorId: String, myQuizzesView: MyQuizzesTableViewController) {
        
        Quizzes.getAllByCreatorId(creatorId: creatorId, onSuccess: { quizzes in
            myQuizzesView.quizzes = quizzes
            myQuizzesView.tableView.reloadData()
        }, onFailure: { error in
            
        })
        
    }
    
}
