//
//  QuizBuilder.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 12/3/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import Foundation

class StudentQuizHandler {
    
    static func getQuizzesToTake(studentId: String, tableView: SavedQuizzesTableViewController) {
        
        EnrollmentHandler.getQuizEnrollmentByStudent(studentId: studentId, onSuccess: { (quizIds, quizTakens) in
            
            for quizId in quizIds {
                Quizzes.getById(id: quizId, onSuccess: { dbQuiz in
                    
                    tableView.savedQuizzes.append(dbQuiz)
                    tableView.tableView.reloadData()
                    
                }, onFailure: { error in
                    print(error.localizedDescription)
                })
            }
            
        }, onFailure: { error in
            print(error.localizedDescription)
        })
    }
    
    static func getTakenStatusOfQuizzes(studentId: String, tableView: SavedQuizzesTableViewController) {
        EnrollmentHandler.getQuizEnrollmentByStudent(studentId: studentId, onSuccess: { (quizIds, quizTakens) in
            
            for taken in quizTakens {
                tableView.takenQuizzes.append(taken)
                tableView.tableView.reloadData()
            }
            
        }, onFailure: { error in
            print(error.localizedDescription)
        })
    }
    
}
