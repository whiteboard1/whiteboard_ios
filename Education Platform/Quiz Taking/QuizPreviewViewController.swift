//
//  QuizPreviewViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 11/30/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit

class QuizPreviewViewController: UIViewController {
    
    var currentQuiz: Quiz = Quiz(name: "", subject: "", creator: "", duration: 0, partialPointsAllowed: false)
    var attempted: Bool = false
    
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var timeLimitLabel: UILabel!
    @IBOutlet weak var openDateLabel: UILabel!
    @IBOutlet weak var closeDateLabel: UILabel!
    @IBOutlet weak var attemptQuizButton: UIButton!
    @IBOutlet weak var reviewQuizButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        
        Questions.getAllByQuiz(quizId: currentQuiz.id!, onSuccess: { questions in
            self.currentQuiz.questions = questions
            var score: Int = 0
            for question in questions {
                score += question.points
            }
            self.pointsLabel.text = String(score)
        }, onFailure: { error in
            print(error.localizedDescription)
        })
        
        subjectLabel.text = currentQuiz.subject
        navigationItem.title = currentQuiz.name
        pointsLabel.text = String(currentQuiz.totalScore)
        timeLimitLabel.text = String(currentQuiz.duration)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy 'at' h:mm a"
        openDateLabel.text = dateFormatter.string(from: currentQuiz.openDate)
        closeDateLabel.text = dateFormatter.string(from: currentQuiz.closeDate)
        
        if attempted {
            reviewQuizButton.isHidden = true
        }
        else {
            attemptQuizButton.isHidden = true
        }
        
    }
    
//    //Incomplete, but good enough for demonstration
    @IBAction func attemptQuizTapped(_ sender: Any) {
        AnswerOptions.getAllByQuestion(questionId: self.currentQuiz.questions[0].id!, onSuccess: { options in
            self.currentQuiz.questions[0].answerOptions = options
            self.performSegue(withIdentifier: "AttemptQuizSegue", sender: self.currentQuiz)
        }, onFailure: { error in
            print(error.localizedDescription)
        })
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let takingQuizVC = segue.destination as? TakingQuizViewController {
            if let selectedQuiz = sender as? Quiz {
                takingQuizVC.quiz = selectedQuiz
            }
        }
    }


}
