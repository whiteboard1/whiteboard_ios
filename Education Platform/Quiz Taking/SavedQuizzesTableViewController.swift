//
//  SavedQuizzesTableViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 11/30/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit
import FirebaseAuth

struct QuizAndTaken {
    var quiz: Quiz
    var taken: Bool
}

class SavedQuizzesTableViewController: UITableViewController {
    
    var savedQuizzes = [Quiz]()
    var takenQuizzes = [Bool]()
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController!.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        StudentQuizHandler.getQuizzesToTake(studentId: Auth.auth().currentUser!.uid, tableView: self)
        StudentQuizHandler.getTakenStatusOfQuizzes(studentId: Auth.auth().currentUser!.uid, tableView: self)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedQuizzes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = savedQuizzes[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedQuiz = savedQuizzes[indexPath.row]
        let selectedIsTaken = takenQuizzes[indexPath.row]
        let quizandt = QuizAndTaken(quiz: selectedQuiz, taken: selectedIsTaken)
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "QuizPreviewSegue", sender: quizandt)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let quizPreviewVC = segue.destination as? QuizPreviewViewController {
            if let selectedQuiz = sender as? QuizAndTaken {
                quizPreviewVC.currentQuiz = selectedQuiz.quiz
                quizPreviewVC.attempted = selectedQuiz.taken
            }
        }
    }
    
}
