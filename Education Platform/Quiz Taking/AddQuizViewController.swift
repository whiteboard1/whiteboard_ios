//
//  AddQuizViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 11/30/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit
import FirebaseAuth

class AddQuizViewController: UIViewController {
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var quizCodeTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        errorLabel.text = ""
    }
    
    @IBAction func addQuizTapped(_ sender: Any) {
        if let enrollmentKey = quizCodeTextField.text, !enrollmentKey.isEmpty {
            EnrollmentHandler.enrollStudentInQuiz(studentId: Auth.auth().currentUser!.uid, enrollmentKey: enrollmentKey, onSuccess: { quiz in
                self.navigationController?.popViewController(animated: true)
            }, onFailure: { error in
                print(error.localizedDescription)
            })
        }
    }
    
    
    
    
}
