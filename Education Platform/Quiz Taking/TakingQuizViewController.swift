//
//  TakingQuizViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 11/30/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit

class AnswerOptionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var AnswerOptionLabel: UILabel!
    @IBOutlet weak var optionLabel: UILabel!
}

class TakingQuizViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var quiz = Quiz(name: "", subject: "", creator: "", duration: 0, partialPointsAllowed: false)
    var quizSubmission = QuizSubmission()
    var answerSubmissions = [AnswerSubmission]()
    
    var currentQuestion: Int = 0
    var answersChosen: Int = 0
    
    @IBOutlet weak var questionNumberLabel: UILabel!
    @IBOutlet weak var questionPointsLabel: UILabel!
    @IBOutlet weak var questionTextField: UITextView!
    @IBOutlet weak var nextQuestionButton: UIBarButtonItem!
    @IBOutlet weak var prevQuestionButton: UIBarButtonItem!
    @IBOutlet weak var quizDoneButton: UIBarButtonItem!
    @IBOutlet weak var answerOptionsTableView: UITableView!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        loadQuestion()
        loadButtons()
        answerOptionsTableView.reloadData()
    }
    
    override func viewDidLoad() {
        answerOptionsTableView.delegate = self
        answerOptionsTableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        quiz.questions[currentQuestion].answerOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Answer Option Cell") as! AnswerOptionTableViewCell
        cell.AnswerOptionLabel.text = quiz.questions[currentQuestion].answerOptions[indexPath.row].text
        cell.optionLabel.text = Utilities.options[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cells = self.answerOptionsTableView.visibleCells
        if(cells[indexPath.row].backgroundColor == UIColor.white && quiz.partialPointsAllowed == false && answersChosen == 0) {
            answersChosen += 1
            cells[indexPath.row].backgroundColor = UIColor.green
        }
        else if(cells[indexPath.row].backgroundColor == UIColor.white && quiz.partialPointsAllowed == true) {
            answersChosen += 1
            cells[indexPath.row].backgroundColor = UIColor.green
        }
        else {
            answersChosen -= 1
            cells[indexPath.row].backgroundColor = UIColor.white
        }
        self.answerOptionsTableView.deselectRow(at: indexPath, animated: true)
    }
    
    func loadQuestion() {
        questionNumberLabel.text = String(currentQuestion + 1)
        questionTextField.text = quiz.questions[currentQuestion].text
        questionPointsLabel.text = String(quiz.questions[currentQuestion].points)
    }
    
    func loadButtons() {
        if(currentQuestion == quiz.questions.count-1) {
            nextQuestionButton.isEnabled = false
            nextQuestionButton.title = ""
            quizDoneButton.isEnabled = true
            quizDoneButton.title = "Done"
        }
        else {
            nextQuestionButton.isEnabled = true
            nextQuestionButton.title = "Next"
            quizDoneButton.isEnabled = false
            quizDoneButton.title = ""
        }
        if(currentQuestion != 0) {
            prevQuestionButton.isEnabled = true
            prevQuestionButton.title = "Previous"
        }
        else {
            prevQuestionButton.isEnabled = false
            prevQuestionButton.title = ""
        }
    }
    
    @IBAction func nextQuestionIsTapped(_ sender: Any) {
        currentQuestion += 1
        let cells = self.answerOptionsTableView.visibleCells
        
        loadQuestion()
        loadButtons()
    }
    
    //Also incomplete, prevents soft-locking
    @IBAction func QuizDoneButtonIsTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
