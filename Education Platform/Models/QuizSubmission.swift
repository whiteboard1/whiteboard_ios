import Foundation

class QuizSubmission {

    var id: String?
    var studentId: String?
    var quizId: String?
    var submittedDate: Date?
    
}
