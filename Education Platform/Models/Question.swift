class Question {

    var id: String?
    var quizId: String?
    var text: String
    var answerOptions: [AnswerOption]
    var points: Int
    
    
    init(text: String, points: Int) {
        self.text = text
        self.answerOptions = []
        self.points = points
    }
    
}
