class AnswerSubmission {

    var id: String?
    var quizSubmissionId: String?
    var questionId: String?
    var submittedAnswer: [String]
    var score: Double
    var timeToAnswer: Int

    init(submittedAnswer: [String], score: Double, timeToAnswer: Int){
        self.submittedAnswer = submittedAnswer
        self.score = score
        self.timeToAnswer = timeToAnswer
    }
}
