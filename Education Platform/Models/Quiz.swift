import Foundation

class Quiz {

    var id: String?
    var creatorId: String?
    var name: String
    var subject: String
    var creator: String
    var questions: [Question]
    var creationDate: Date?
    var duration: Int
    var totalScore: Int
    var openDate: Date
    var closeDate: Date
    var partialPointsAllowed: Bool
    var enrollmentKey: String?

    
    init(name: String, subject: String, creator: String, duration: Int, partialPointsAllowed: Bool) {
        self.name = name
        self.subject = subject
        self.questions = []
        self.openDate = Date()
        self.closeDate = Date()
        self.totalScore = 0
        self.creator = creator
        self.duration = duration
        self.partialPointsAllowed = partialPointsAllowed
    }
    
}
