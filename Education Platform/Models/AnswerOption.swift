class AnswerOption {

    var id: String?
    var questionId: String?
    var text: String
    var isCorrect: Bool
    var explanation: String

    init(text: String, isCorrect: Bool, explanation: String){
        self.text = text
        self.isCorrect = isCorrect
        self.explanation = explanation
    }
}