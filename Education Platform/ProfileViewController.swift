//
//  ProfileViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 10/2/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var buttons : [String] = ["Change Email", "Change Password"]
    
    let auth = AuthService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        auth.getProfileInto(profileClass: self)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isScrollEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController!.navigationItem.setHidesBackButton(true, animated: false)
        self.tabBarController!.navigationItem.title = "Profile"
        
        self.tabBarController!.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        auth.signOut()
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buttons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        cell.textLabel?.text = buttons[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
