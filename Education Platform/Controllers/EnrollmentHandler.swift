import FirebaseFirestore

class EnrollmentHandler {

    static var KEY_COLLECTION = "keys"
    static var ENROLLMENT_COLLECTION = "enrollment"
    static var KEY_INFO = "key_info"
    static var GLOBAL_COUNTER = "global_counter"
    static var QUIZ_ID = "quiz_id"
    static var ENROLLED_QUIZZES = "enrolled_quizzes"
    static var TAKEN = "taken"
    static var hashids = Hashids(salt: "com.whiteboard.eduani", minHashLength:6)

    static func generateEnrollmentKey(quizId: String, onSuccess: @escaping (String) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        let reference = db.collection(KEY_COLLECTION).document(KEY_INFO)
        db.runTransaction({ (transaction, errorPointer) -> Any? in 
            let infoDoc: DocumentSnapshot
            do{
                try infoDoc = transaction.getDocument(reference)
            } catch let fetchError as NSError {
                errorPointer?.pointee = fetchError
                return nil
            }

            guard let globalCounter = infoDoc.data()?[GLOBAL_COUNTER] as? Int else {
                let error = NSError(domain: "whiteboard", code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not get global counter"])
                errorPointer?.pointee = error
                return nil
            }

            transaction.updateData([GLOBAL_COUNTER: globalCounter+1], forDocument: reference)

            let enrollmentKey = hashids.encode(globalCounter)
            let keyRef = db.collection(KEY_COLLECTION).document(enrollmentKey!)
            transaction.setData([QUIZ_ID: quizId], forDocument: keyRef)
            
            let q = Quiz(name: "", subject: "", creator: "", duration: 0, partialPointsAllowed: false)
            q.id = quizId
            q.enrollmentKey = enrollmentKey
            Quizzes.update(quiz: q, parameters: [Quizzes.ENROLLMENT_KEY], onSuccess: {}, onFailure: {err in
                print(err.localizedDescription)
            })
            return enrollmentKey
        }) { (enrollmentKey, error) in
            if let err = error {
                onFailure(err)
            } else {
                onSuccess(enrollmentKey as! String)
            }
        }
    }

    static func getQuizEnrollmentByStudent(studentId: String, onSuccess: @escaping ([String], [Bool]) -> Void, onFailure: @escaping (Error) -> Void){
        let db = Firestore.firestore()

        db.collection(ENROLLMENT_COLLECTION).document(studentId)
            .getDocument { (document, error) in
                if let err = error {
                    onFailure(err)
                    return
                }
                if let document = document, document.exists {
                    let enrolledQuizzes = document.data()?[ENROLLED_QUIZZES] as? [[String : Any]]
                    var quizIds: [String] = []
                    var takens: [Bool] = []

                    for e in enrolledQuizzes! {
                        quizIds.append(e[QUIZ_ID] as! String)
                        takens.append((e[TAKEN] != nil))
                    }
                    onSuccess(quizIds, takens)
                }
                else {
                    onSuccess([],[])
                }
            }
    }

    static func enrollStudentInQuiz(studentId: String, enrollmentKey: String, onSuccess: @escaping (Quiz) -> Void, onFailure: @escaping (Error) -> Void) {
        let db = Firestore.firestore()

        let reference = db.collection(KEY_COLLECTION).document(enrollmentKey)
        reference.getDocument { (document, error) in 
                if let document = document, document.exists {

                    let quizId = document.data()?[QUIZ_ID] as? String
                    let quizRef = db.collection(Quizzes.QUIZ_COLLECTION).document(quizId!)

                    quizRef.getDocument { (document, error) in 
                        if let quizDoc = document, document!.exists {
                            
                            var quiz = Quiz(name: quizDoc[Quizzes.NAME] as! String, subject: quizDoc[Quizzes.SUBJECT] as! String, creator: quizDoc[Quizzes.CREATOR] as! String, duration: quizDoc[Quizzes.DURATION] as! Int, partialPointsAllowed: ((quizDoc[Quizzes.PARTIAL_POINTS_ALLOWED]) != nil))
                            quiz.openDate = (quizDoc[Quizzes.OPEN_DATE] as! Timestamp).dateValue()
                            quiz.closeDate = (quizDoc[Quizzes.CLOSE_DATE] as! Timestamp).dateValue()
                            quiz.id = quizDoc.documentID
                            quiz.creatorId = quizDoc[Quizzes.CREATOR_ID] as! String

                            let enrollRef = db.collection(ENROLLMENT_COLLECTION).document(studentId)
                            enrollRef.setData([ENROLLED_QUIZZES: FieldValue.arrayUnion([[QUIZ_ID: quizId, TAKEN: false]])]) {err in
                                if let err = err {
                                    onFailure(err)
                                } else {
                                    onSuccess(quiz)
                                }
                            }

                        } else {
                            onFailure(error!)
                        }
                    }
                } else {
                    onFailure(error!)
                }
            }

        // db.runTransaction({ (transaction, errorPointer) -> Any? in 
        //     let keyDoc: DocumentSnapshot
        //     do{
        //         try keyDoc = transaction.getDocument(reference)
        //     } catch let fetchError as NSError {
        //         errorPointer?.pointee = fetchError
        //         return nil
        //     }

        //     guard let quizId = keyDoc.data()?[QUIZ_ID] as? String else {
        //         let error = NSError(domain: "whiteboard", code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not get quiz id"])
        //         errorPointer?.pointee = error
        //         return nil
        //     }

        //     let quizRef = db.collection(Quizzes.QUIZ_COLLECTION).document(quizId)
        //     var quizDoc: DocumentSnapshot?
        //     do{
        //         try quizDoc = transaction.getDocument(quizRef)
        //     } catch let fetchError as NSError {
        //         errorPointer?.pointee = fetchError
        //         return nil
        //     }
            
        //     let enrollRef = db.collection(ENROLLMENT_COLLECTION).document(studentId)
        //     transaction.updateData([ENROLLED_QUIZZES: FieldValue.arrayUnion([[QUIZ_ID: quizId, TAKEN: false]])], forDocument: enrollRef)

        //     var quiz = Quiz(name: quizDoc?[Quizzes.NAME] as! String, subject: quizDoc?[Quizzes.SUBJECT] as! String, creator: quizDoc?[Quizzes.CREATOR] as! String, duration: quizDoc?[Quizzes.DURATION] as! Int, partialPointsAllowed: ((quizDoc?[Quizzes.PARTIAL_POINTS_ALLOWED]) != nil))
        //     quiz.openDate = (quizDoc?[Quizzes.OPEN_DATE] as! Timestamp).dateValue()
        //     quiz.closeDate = (quizDoc?[Quizzes.CLOSE_DATE] as! Timestamp).dateValue()
        //     quiz.id = quizDoc?.documentID
        //     quiz.creatorId = quizDoc?[Quizzes.CREATOR_ID] as! String
            
        //     return quiz
        // }) { (quiz, error) in
        //     if let err = error {
        //         onFailure(err)
        //     } else {
        //         onSuccess(quiz as! Quiz)
        //     }
        // }
    }

    static func setEnrolledQuizAsTaken(quizId: String, studentId: String, onSuccess: @escaping () -> Void, onFailure: @escaping (Error) -> Void) {
        let db = Firestore.firestore()
        let enrollRef = db.collection(ENROLLMENT_COLLECTION).document(studentId)
        enrollRef.setData([ENROLLED_QUIZZES: FieldValue.arrayRemove([[QUIZ_ID: quizId, TAKEN: false]])]) {err in
            if let err = err {
                onFailure(err)
            } else {
                enrollRef.setData([ENROLLED_QUIZZES: FieldValue.arrayUnion([[QUIZ_ID: quizId, TAKEN: true]])]) {err in
                    if let err = err {
                        onFailure(err)
                    } else {
                        onSuccess()
                    }
                }
            }
        }
    }
}
