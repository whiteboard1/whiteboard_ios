//
//  Answer.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 11/2/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import Foundation

class Answer {
    
    var answer: String
    var explanation: String
    var isCorrect: Bool
    
    init() {
        answer = String()
        explanation = String()
        isCorrect = false
    }
}
