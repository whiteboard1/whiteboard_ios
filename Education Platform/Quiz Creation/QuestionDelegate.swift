//
//  QuestionDelegate.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 11/2/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import Foundation

protocol QuestionDelegate {
    func onQuestionDone(question: Question)
}
