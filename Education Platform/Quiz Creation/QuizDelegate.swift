//
//  QuizDelegate.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 12/1/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import Foundation

protocol QuizDelegate {
    func onQuizDone(quiz: Quiz)
}
