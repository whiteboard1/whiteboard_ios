//
//  MyQuizzesTableViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 10/31/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit
import FirebaseAuth

class QuizTableViewCell: UITableViewCell {
    
    @IBOutlet weak var quizNameLabel: UILabel?
    @IBOutlet weak var EnrolmentKeyLabel: UILabel?
    
}

class MyQuizzesTableViewController: UITableViewController {
    
    var quizzes: [Quiz] = [Quiz]()
    var quizToEditIndex: Int = 0
    var editingQuiz: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController!.navigationController?.setNavigationBarHidden(true, animated: false)
        InstructorQuizHandler.getCreatedQuizzes(creatorId: Auth.auth().currentUser!.uid, myQuizzesView: self)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return quizzes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = UITableViewCell()
//        cell.textLabel?.text = quizzes[indexPath.row].name
//        return cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "Quiz Cell") as! QuizTableViewCell
        cell.quizNameLabel?.text = quizzes[indexPath.row].name
        if let enrollmentKey = quizzes[indexPath.row].enrollmentKey, !enrollmentKey.isEmpty {
            cell.EnrolmentKeyLabel?.text = enrollmentKey
        }
        else {
            cell.EnrolmentKeyLabel?.text = ""
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedQuiz = quizzes[indexPath.row]
        quizToEditIndex = indexPath.row
        editingQuiz = true
        performSegue(withIdentifier: "editQuizSegue", sender: selectedQuiz)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "newQuizSegue":
                break
            case "editQuizSegue":
                if let editQuizVc = segue.destination as? NewQuizViewController {
                    editQuizVc.quizToEditIndex = quizToEditIndex
                    if let selectedQuiz = sender as? Quiz {
                        editQuizVc.quiz = selectedQuiz
                        editQuizVc.questions = selectedQuiz.questions
                        editQuizVc.edit = true
                    }
                }
            default:
                print("Error")
            }
            
        }
    }
    
}
