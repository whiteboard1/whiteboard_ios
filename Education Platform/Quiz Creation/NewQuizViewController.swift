//
//  NewQuizViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 10/31/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit
import FirebaseAuth

class NewQuizViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, QuestionDelegate {
    
    var quiz: Quiz = Quiz(name: "", subject: "", creator: (Auth.auth().currentUser?.displayName)!, duration: 0, partialPointsAllowed: false)
    var questions: [Question] = []
    var questionToEditIndex: Int = 0
    var quizToEditIndex: Int = 0
    var editingQuestion: Bool = false
    var edit: Bool = false
    var score = 0
    
    @IBOutlet weak var makeQuizAvailableButton: UIBarButtonItem!
    @IBOutlet weak var questionsTableView: UITableView!
    @IBOutlet weak var quizNameTextField: UITextField!
    @IBOutlet weak var subjectTextField: UITextField!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var timeLimitTextField: UITextField!
    @IBOutlet weak var opensAtTextField: UITextField!
    @IBOutlet weak var closesAtTextField: UITextField!
    @IBOutlet weak var partialPointsSwitch: UISwitch!
    
    private var openDatePicker: UIDatePicker?
    private var closeDatePicker: UIDatePicker?
    
    let alert = UIAlertController(title: "Make Quiz Available?", message: "You can edit quizzes after making them available.", preferredStyle: .alert)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questionsTableView.delegate = self
        questionsTableView.dataSource = self
        quizNameTextField.delegate = self
        subjectTextField.delegate = self
        timeLimitTextField.delegate = self
        opensAtTextField.delegate = self
        closesAtTextField.delegate = self
        
        openDatePicker = UIDatePicker()
        openDatePicker?.datePickerMode = .dateAndTime
        opensAtTextField.inputView = openDatePicker
        openDatePicker?.addTarget(self, action: #selector(NewQuizViewController.openDateChanged(datePicker:)), for: .valueChanged)
        openDateChanged(datePicker: openDatePicker!)
        
        closeDatePicker = UIDatePicker()
        closeDatePicker?.datePickerMode = .dateAndTime
        closesAtTextField.inputView = closeDatePicker
        closeDatePicker?.addTarget(self, action: #selector(NewQuizViewController.closeDateChanged(datePicker:)), for: .valueChanged)
        closeDateChanged(datePicker: closeDatePicker!)
        
        quiz.creatorId = Auth.auth().currentUser?.uid
        if edit {
            quizNameTextField.text = quiz.name
            subjectTextField.text = quiz.subject
            timeLimitTextField.text = String(quiz.duration)
            opensAtTextField.text = convertString(date: quiz.openDate)
            closesAtTextField.text = convertString(date: quiz.closeDate)
            partialPointsSwitch.setOn(quiz.partialPointsAllowed, animated: false)
            Questions.getAllByQuiz(quizId: quiz.id!, onSuccess: { allQuestions in
                self.questions = allQuestions
                self.questionsTableView.reloadData()
                self.scoreLabel.text = String(self.updateScoreLabel())
            }, onFailure: {error in
                print(error.localizedDescription)
            })
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                EnrollmentHandler.generateEnrollmentKey(quizId: self.quiz.id!, onSuccess: {key in
                    self.quiz.enrollmentKey = key
                    self.navigationController?.popViewController(animated: true)
                }, onFailure: { error in
                    print(error.localizedDescription)
                })
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        }
        else {
            makeQuizAvailableButton.isEnabled = false
            makeQuizAvailableButton.image = nil
            makeQuizAvailableButton.title = ""
        }
        
        self.addDoneButtonOnKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController!.navigationController?.setNavigationBarHidden(true, animated: false)
        editingQuestion = false
        questionsTableView.reloadData()
        score = 0;
        for question in questions {
            score += question.points
        }
        scoreLabel.text = String(score)
        if edit {
            navigationItem.title = "Edit Quiz"
        }
        else {
            navigationItem.title = "New Quiz"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = questions[indexPath.row].text
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedQuestion = questions[indexPath.row]
        questionToEditIndex = indexPath.row
        editingQuestion = true
        performSegue(withIdentifier: "editQuestionSegue", sender: selectedQuestion)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            questions.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            score = 0;
            for question in questions {
                score += question.points
            }
            scoreLabel.text = String(score)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func onQuestionDone(question: Question) {
        if editingQuestion {
            questions[questionToEditIndex] = question
        }
        else {
            questions.append(question)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "newQuestionSegue":
                if let newQuestionVc = segue.destination as? NewQuestionViewController {
                    newQuestionVc.delegate = self
                }
            case "editQuestionSegue":
                if let editQuestionVc = segue.destination as? NewQuestionViewController {
                    editQuestionVc.delegate = self
                    editQuestionVc.questionToEditIndex = questionToEditIndex
                    if let selectedQuestion = sender as? Question {
                        editQuestionVc.question = selectedQuestion
                        editQuestionVc.numberOfAnswers = selectedQuestion.answerOptions.count
                        editQuestionVc.edit = true
                    }
                }
            default:
                print("Error") //shouldn't get here
            }
            
        }
    }
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        timeLimitTextField.inputAccessoryView = doneToolbar
        opensAtTextField.inputAccessoryView = doneToolbar
        closesAtTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        timeLimitTextField.resignFirstResponder()
        opensAtTextField.resignFirstResponder()
        closesAtTextField.resignFirstResponder()
    }
    
    @objc func openDateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy 'at' h:mm a"
        opensAtTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    @objc func closeDateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy 'at' h:mm a"
        closesAtTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    func convertDate(date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy 'at' h:mm a"
        return dateFormatter.date(from: date)!
    }
    
    func convertString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy 'at' h:mm a"
        return dateFormatter.string(from: date)
    }
    
    @IBAction func saveQuizButtonTapped(_ sender: Any) {
        quiz.name = quizNameTextField.text!
        quiz.subject = subjectTextField.text!
        quiz.totalScore = Int(scoreLabel.text!)!
        quiz.duration = Int(timeLimitTextField.text!)!
        quiz.openDate = convertDate(date: opensAtTextField.text!)
        quiz.closeDate = convertDate(date: closesAtTextField.text!)
        quiz.partialPointsAllowed = partialPointsSwitch.isOn
        quiz.questions = questions
        
        if !edit {
            Quizzes.add(quiz: quiz, onSuccess: { quizWithId in
                for question in quizWithId.questions {
                    question.quizId = quizWithId.id
                    Questions.add(question: question, onSuccess: { questionWithId in
                        for option in questionWithId.answerOptions {
                            option.questionId = questionWithId.id
                            AnswerOptions.add(ansOpt: option, onSuccess: { optionWithId in
                                
                            }, onFailure: { optionError in
                                print("Answer Option")
                                print(optionError.localizedDescription)
                            })
                        }
                    }, onFailure: { questionError in
                        print("Question")
                        print(questionError.localizedDescription)
                    })
                }
                self.navigationController?.popViewController(animated: true)
            }, onFailure: { quizError in
                print("Quiz")
                print(quizError.localizedDescription)
                return
            })
        }
        else {
            Quizzes.update(quiz: quiz, parameters: nil, onSuccess: {
                for question in self.quiz.questions {
                    Questions.update(question: question, parameters: nil, onSuccess: {
                        for option in question.answerOptions {
                            AnswerOptions.update(ansOpt: option, parameters: nil, onSuccess: {}, onFailure: { error in
                                print(error.localizedDescription)
                            })
                        }
                    }, onFailure: {error in
                        print(error.localizedDescription)
                    })
                }
                self.navigationController?.popViewController(animated: true)
            }, onFailure: { error in
                print(error.localizedDescription)
                return
            })
            
        }
        
    }
    
    @IBAction func makeQuizAvailableTapped(_ sender: Any) {
        self.present(alert, animated: true)
    }
    
    
    func updateScoreLabel() -> Int{
        var newScore: Int = 0
        for question in questions {
            newScore += question.points
        }
        return newScore
    }
    
}
