//
//  File.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 10/31/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import Foundation

class Question {
    var id: String
    var answers: [Answer]
    var question: String
    var subject: String
    var points: Int
    
    init() {
        id = String()
        answers = []
        question = String()
        subject = String()
        points = 0
    }
}
