//
//  NewQuestionViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 10/31/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit

class AnswerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var answerNumberLabel: UILabel!
    @IBOutlet weak var answerTextVIew: UITextView!
    @IBOutlet weak var explanationTextView: UITextView!
    @IBOutlet weak var isCorrectSwitch: UISwitch!
}

class NewQuestionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var numberOfAnswers: Int = 0;
    var question: Question = Question(text: "", points: 0)
    var answers: [AnswerOption] = []
    var edit: Bool = false
    var delegate: QuestionDelegate?
    var questionToEditIndex: Int = 0
    
    @IBOutlet weak var questionTextView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pointsTextField: UITextField!
    @IBOutlet weak var answersBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        pointsTextField.text = String(question.points)
        questionTextView.text = question.text
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController!.navigationController?.setNavigationBarHidden(true, animated: false)
        if edit {
            navigationItem.title = "Edit Question"
            AnswerOptions.getAllByQuestion(questionId: question.id!, onSuccess: { options in
                self.answers = options
                self.question.answerOptions = options
                self.numberOfAnswers = self.answers.count
                self.tableView.reloadData()
            }, onFailure: { error in
                print(error.localizedDescription)
            })
        }
        else {
            navigationItem.title = "New Question"
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfAnswers
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Answer Cell") as! AnswerTableViewCell
        //cell.answerNumberLabel.text =  String(indexPath.row + 1)
        if(indexPath.row < question.answerOptions.count) {
            cell.answerTextVIew.text = question.answerOptions[indexPath.row].text
            cell.explanationTextView.text = question.answerOptions[indexPath.row].explanation
            cell.isCorrectSwitch.isOn = question.answerOptions[indexPath.row].isCorrect
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.answersBottomConstraint.constant = 0
            numberOfAnswers -= 1
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    @IBAction func addTapped(_ sender: Any) {
        numberOfAnswers = numberOfAnswers + 1
        self.answersBottomConstraint.constant = 0
        tableView.insertRows(at: [IndexPath(row: numberOfAnswers-1, section: 0)], with: .automatic)
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        let cells = self.tableView.visibleCells as! Array<AnswerTableViewCell>
        for cell in cells {
            let answer = AnswerOption(text: cell.answerTextVIew.text, isCorrect: cell.isCorrectSwitch.isOn, explanation: cell.explanationTextView.text)
            answers.append(answer)
        }
        question.answerOptions = answers
        if let questionText = questionTextView.text {
            question.text = questionText
        }
        if let points = pointsTextField.text {
            if let pointNumber = Int(points) {
                question.points = pointNumber
            }
        }
        delegate?.onQuestionDone(question: question)
        navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let info = notification.userInfo {
            let rect:CGRect = info["UIKeyboardFrameEndUserInfoKey"] as! CGRect
            
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
                self.answersBottomConstraint.constant = rect.height - 75
            }
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        self.answersBottomConstraint.constant = 0
    }
}
