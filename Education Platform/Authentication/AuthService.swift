//
//  AuthService.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 10/28/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class AuthService {
    
    //Dummy initializers
    var signupView: SignupViewController? = nil
    var loginView: LoginViewController? = nil
    
    /********************************************************************/
    /******                                                 Error Constants                                                  ******/
    /********************************************************************/
    
    private let OK: Int = 0
    private let EMAIL_FORMAT: Int = 1
    private let PASSWORD_UPPER: Int = 2
    private let PASSWORD_LOWER: Int = 3
    private let PASSWORD_SPEC: Int = 4
    private let PASSWORD_MATCH: Int = 5
    private let WRONG_LOGIN: Int = 6
    private let NO_USER_FOUND: Int = 7
    private let FILL_FIELDS: Int = 8
    private let PASSWORD_LENGTH: Int = 9
    
    /********************************************************************/
    /******                                                         signup                                                         ******/
    /********************************************************************/
    
    func signUp(signupClass: SignupViewController, name: String, email: String, password: String) {
        signupView = signupClass
        
        if let confirmPassword = signupView?.confirmPasswordTextField.text {
            if email.isEmpty || password.isEmpty || name.isEmpty || confirmPassword.isEmpty {
                signupView?.errorLabel.text = Utilities.errors[FILL_FIELDS]
                return
            }
            
            //correct password format
            let passwordError = passwordIsValid(password)
            if passwordError != OK {
                signupView?.errorLabel.text = Utilities.errors[passwordError]
                return
            }
            
            //passwords match
            if password != confirmPassword {
                signupView?.errorLabel.text = Utilities.errors[PASSWORD_MATCH]
                return
            }
        }
        
//        if(!emailIsValid(email)) {
//            print(email)
//            signupView?.errorLabel.text = Utilities.errors[EMAIL_FORMAT]
//            return
//        }
        
        createUser(email: email, password: password)
    }
    
    private func createUser(email: String, password: String, _ callback: ((Error?) -> ())? = nil){
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let e = error {
                //probably wrong email or password errors from firebase, shouldn't get to this.
                print(e.localizedDescription)
            }
            else {
                if let currentView = self.signupView {
                    if let fullNameText = currentView.fullNameTextField.text, !fullNameText.isEmpty{
                        self.createProfileChangeRequest(name: fullNameText)
                        let db = Firestore.firestore()
                        
                        db.collection("users").addDocument(data: ["email":email, "full_name":fullNameText]) { (error) in
                            if error != nil {
                                currentView.errorLabel.text = "Error saving user data." //also shouldn't happen
                            }
                        }
                    }
                    currentView.performSegue(withIdentifier: "moveToEnrolledCourses", sender: self)
                }
            }
        }
    }
    
    /********************************************************************/
    /******                                                           login                                                          ******/
    /********************************************************************/
    
    func logIn(loginClass: LoginViewController, email: String, password: String) {
        loginView = loginClass
        login(withEmail: email, password: password)
    }
    
    private func login(withEmail email: String, password: String, _ callback: ((Error?) -> ())? = nil){
        
        if email.isEmpty || password.isEmpty {
            if let currentView = self.loginView {
                currentView.errorLabel.text = Utilities.errors[FILL_FIELDS]
            }
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let currentView = self.loginView {
                if error != nil {
                    currentView.errorLabel.text = Utilities.errors[self.WRONG_LOGIN]
                }
                else {
                    currentView.performSegue(withIdentifier: "moveToEnrolledCourses", sender: self)
                }
            }
        }
    }
    
    func logBackIn(loginClass: LoginViewController) {
        if(Auth.auth().currentUser != nil) {
            loginClass.performSegue(withIdentifier: "moveToEnrolledCourses", sender: self)
        }
    }
    
    /********************************************************************/
    /******                                                    profile actions                                                   ******/
    /********************************************************************/
    
    func signOut(){
        try? Auth.auth().signOut()
    }
    
    func getProfileInto(profileClass: ProfileViewController) {
        profileClass.fullNameLabel.text = Auth.auth().currentUser?.displayName
        profileClass.emailLabel.text = Auth.auth().currentUser?.email
    }
    
    /********************************************************************/
    /******                          change profile pic and add name to database                           ******/
    /********************************************************************/
    
    func createProfileChangeRequest(photoUrl: URL? = nil, name: String? = nil, _ callback: ((Error?) -> ())? = nil){
        if let request = Auth.auth().currentUser?.createProfileChangeRequest(){
            if let name = name{
                request.displayName = name
            }
            if let url = photoUrl{
                request.photoURL = url
            }
            
            request.commitChanges(completion: { (error) in
                callback?(error)
            })
        }
    }
    
    /********************************************************************/
    /******                                                  field validation                                                    ******/
    /********************************************************************/
    
    private func passwordIsValid(_ password : String) -> Int{
        if password.count < 8 || password.count > 20 {
            return PASSWORD_LENGTH
        }
        if password.range(of: "[A-Z]", options: .regularExpression) == nil {
            return PASSWORD_UPPER
        }
        if password.range(of: "[a-z]", options: .regularExpression) == nil {
            return PASSWORD_LOWER
        }
        if password.range(of: "[!@#$%^&*]", options: .regularExpression) == nil {
            return PASSWORD_SPEC
        }
        return OK
    }
    
    private func emailIsValid(_ email: String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{4,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
