//
//  LoginViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 10/2/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    let auth = AuthService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isToolbarHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        auth.logBackIn(loginClass: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        errorLabel.text = ""
        emailTextField.text = ""
        passwordTextField.text = ""
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        errorLabel.text = ""
        //for now only check if text fields have info
        if let emailText = emailTextField.text, !emailText.isEmpty {
            if let passwordText = passwordTextField.text, !passwordText.isEmpty {
                auth.logIn(loginClass: self, email: emailText, password: passwordText)
                return
            }
        }
        errorLabel.text = "You must fill in every field."
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
