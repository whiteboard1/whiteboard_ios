//
//  SignupViewController.swift
//  Education Platform
//
//  Created by Aníbal Pagán Ventura on 10/2/19.
//  Copyright © 2019 Aníbal Pagán Ventura. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var professorSwitch: UISwitch!
    
    let auth: AuthService = AuthService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        errorLabel.text = ""
        emailTextField.text = ""
        fullNameTextField.text = ""
        passwordTextField.text = ""
        confirmPasswordTextField.text = ""
    }
    
    @IBAction func signupTapped(_ sender: Any) {
        errorLabel.text = ""
        if let emailText = emailTextField.text {
            if let passwordText = passwordTextField.text {
                if let fullNameText = fullNameTextField.text {
                    
                    //correct signup
                    auth.signUp(signupClass: self, name: fullNameText, email: emailText, password: passwordText)
                    
                    return
                }
            }
        }
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
